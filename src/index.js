import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

const SQUARE_CLASS_NAME = "square"; // для манипулирования квадратиками, будет использовать это имя класса

function AutoplayButton(props){ // возвращает код кнопки, которорая будет запускать автоматическую игру
    // имеет единственное свойство onClick - функция, которая будет запускаться при нажатии (код прописан в Game)
    return  (
        <button className="button" onClick={props.onClick}>Автоматическая игра</button>
    );
}

function StartButton(props){ // возвращает код кнопки для запуска игры
    return  (
        <button className="button" onClick={props.onClick}>Начать игру</button>
    );
}

class Timer extends React.Component{ // компонент, который отсчитывает время
    // его состояние определяется целым числом, которое показывает, сколько секунд осталось до конца игры
    constructor(props) {
        super(props);
        this.state = {time: props.time}; // свойство time задаёт начальное состояние
    }

    componentDidMount() { // как только время отображается, счёт пошёл
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        if (this.state.time === 1) { // если последняя секунда прошла
            this.props.timeout(); // вызывает функцию timeout
            return;
        }
        this.setState(state => ({ // уменьшает время на 1 секунду
            time: state.time - 1,
        }));
    }

    render() { // постоянно показывает, сколько времени осталось
        return(
            <p className="timer">осталось {this.state.time} сек.</p>
        );
    }
}

function Square(props){ // возвращает код квадратика
    // noinspection CheckTagEmptyBody
    return  (
        <td className={props.className} style={props.style} onClick={props.onClick}></td>
    );
}


class Board extends React.Component{
    render() {
        const board_elements = []; // массив элементов доски
        for (let i = 0; i < this.props.size; i++) { // построчно создаём доску
            let row = [];
            for (let j = 0; j < this.props.size; j++) { // поэлементное создание строки
                // каждый элемент строки - квадрат
                row.push(<Square className={SQUARE_CLASS_NAME} style={this.props.style} onClick={this.props.onClick} key={j} />)
            }
            // особый квадрат помечен свойствами i, j
            if (i === this.props.i) {
                row[this.props.j] = <Square className={SQUARE_CLASS_NAME} style={this.props.specialStyle} onClick={this.props.onSpecialClick} key={this.props.j}/>
            }
            board_elements.push(<tr key={i}>{row}</tr>)
        }
        return <table className="board"><tbody>{board_elements}</tbody></table>;
    }
}

class Game extends React.Component{

    clickEvent = new Event("click", {bubbles: true});

    handleSpecialClick = () => {
        this.setState(state => ({
            round: state.round + 1,
            mainState: state.mainState,
        }));
    };

    handleClick = () => {
        this.setState(state => ({
            mainState: "fail",
            message: "Вы проиграли, но дошли до " + state.round + "-го уровня"
        }));
    };

    start = () => {
        this.setState({
            round: 1,
            mainState: "running",
            autoplay: false
        });
    };

    autoplay = () => {
        this.setState({
            round: 1,
            mainState: "running",
            autoplay: true
        });
    };

    timeout = () => {
        this.setState(state => ({
            mainState: "timeout",
            message: "Время вышло. Вы дошли до " + state.round + "-го уровня"
        }));
    };

    findSpecialSquareAndClick = () => {
        let squares = document.getElementsByClassName(SQUARE_CLASS_NAME);
        if (squares.length < 4) { // клеток должно быть минимум 4
            return;
        }
        const firstColor = squares[0].style.backgroundColor;
        let specialSquare;
        if (firstColor === squares[1].style.backgroundColor) {
            for (let i = 2; i < squares.length; i++) {
                if (firstColor !== squares[i].style.backgroundColor)
                    specialSquare = squares[i];
            }
        }
        else {
            specialSquare = firstColor === squares[2].style.backgroundColor ? squares[1] : squares[0];
        }
        if (typeof specialSquare !== "undefined") {
            specialSquare.dispatchEvent(this.clickEvent);
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            message: 'Добро пожаловать в игру "Разноцветные квадратики"'
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.mainState === "running" && this.state.autoplay) {
            if (typeof this.timerID !== "undefined")
                clearTimeout(this.timerID);
            this.timerID = setTimeout(this.findSpecialSquareAndClick, 1000);
        }
    }

    random(a, b) {
        return Math.abs(Math.round(Math.random() * (b - a + 1) - 0.5));
    }

    randomColor(limit = 255) { // limit (0-255) задаёт ограничение на максимальную яркость случайного цвета
        return [this.random(0, limit), this.random(0, limit), this.random(0, limit)];
    }

    changeColor(color_settings, delta) {
        let new_settings = color_settings.slice();
        for (let i = 0; i < new_settings.length; i++) {
            new_settings[i] += delta;
        }
        return new_settings;
    }

    colorToString(color_settings) {
        return "rgb(" + color_settings.join(",") + ")";
    }

    createStyle(color_settings) {
        return {
            backgroundColor: this.colorToString(color_settings),
        }
    }

    render() {
        const step = 10; // насколько сильно должен изменяться цвет с каждым шагом?
        const levels_count = 9; // количество уровней сложности
        // начиная с уровня levels_count, сложность будет максимальной
        // delta уменьшается с levels_count * step до step
        const delta = step * (levels_count - Math.min(this.state.round, levels_count) + 1); // сколько шагов отделяют пользователя от самого высокого уровня сложности?
        const color = this.randomColor(255 - delta); // в какой цвет будут покрашены большинство ячеек?
        const specialColor = this.changeColor(color, delta); // в какой цвет будет покрашена особая ячейка?
        const style = this.createStyle(color);
        const specialStyle = this.createStyle(specialColor);

        if (this.state.mainState === "running") { // состояние: игра запущена
            return <div>
                <h1 className="level">{this.state.round} уровень</h1>
                <Timer time={60} timeout={this.timeout}/>
                <Board size={this.state.round + 1} style={style} specialStyle={specialStyle} i={this.random(0, this.state.round)} j={this.random(0,this.state.round)} onClick={this.handleClick} onSpecialClick={this.handleSpecialClick}/>
            </div>;
        }
        // состояние: игра ещё не запущена
        return <div>
            <StartButton onClick={this.start}/>
            <AutoplayButton onClick={this.autoplay}/>
            <h1 className="message">{this.state.message}</h1>
        </div>
    }
}


// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root')
);
